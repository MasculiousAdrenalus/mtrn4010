function project2_2
global ABCD;
ABCD.flagPause=0;
ABCD.flagEnd = 0;
% In case the caller does not specify the input argument, we propose a
% default one, assumed to be in the same folder where we run this example from.

%Load the IMU data   Load Speed data           Load Laser data
load('IMU_dataC','IMU');   load('Speed_dataC.mat','Vel');  load('Laser__2C.mat','dataL');


%Extract the IMU data
%First time must be changed from counts to seonds
IMUTime = double(IMU.times);
IMUTime = IMUTime - IMUTime(1); %gives us the number of counts
IMUTime = IMUTime/10000; %this gives you the time in seconds

%Do the same for the laser data
Laser_time = double(dataL.times);
Laser_time = Laser_time - Laser_time(1);
Laser_time = Laser_time/10000;

%Yaw data must be negated due to its direction
Yaw = -IMU.DATAf(6,:);

%Find the bias by calculating the average for the first 10 secs

bias = mean(Yaw(1:1000));
Yaw = Yaw - bias;

%Obtain a matrix for the X & Y coordinates
Speed = Vel.speeds;
X = zeros(Vel.N,1);
Y = zeros(Vel.N,1);

%Initialise the starting angle according to the criteria
angle = zeros(IMU.N,1);
angle(1) = pi/2;

% --------------------------------------
% Create graphical object for refreshing data during program execution.
figure(1) ; clf();

MyGUIHandles.handle1 = plot(0,0,'b.');
hold on;% to be used for showing the laser points
MyGUIHandles.handle2 = plot(0,0,'r+');
MyGUIHandles.handle3 = plot(0,0, 'g*');
axis([-10,10,-10,10]);                         % focuses the plot on this region (of interest, close to the robot)
xlabel('X (meters)');
ylabel('Y (meters)');

MyGUIHandles.handle4 = title('');           % create an empty title..
zoom on ;  grid on;

figure(2); clf();
grid on; hold on;
MyGUIHandles.handle5 = plot(0,0,'m.');
MyGUIHandles.handle6 = plot(0,0,'c*');
MyGUIHandles.handle7 = plot(0,0,'g o');
axis([-8,8,-8,8]);
% If you do not understand these functions ( figure() ,plot(),
% axis(),.....) ===> Read Matlab's Help.
% (In MTRN2500 you used them)

%---------------------------------

disp('Showing laser scans, in Cartesian representation');

fprintf('\nThere are [ %d ] laser scans in this dataset (file [%s])\n',dataL.N);


uicontrol('Style','pushbutton','String','Pause/Cont.','Position',[10,1,80,20],'Callback',{@MyCallBackA,1});
uicontrol('Style','pushbutton','String','END Now','Position',[90,1,80,20],'Callback',{@MyCallBackA,2});



% Now, loop through the avaialable scans..

N = dataL.N;
skip=1;
i=1;
j =1;

        t =  Laser_time(j);
        % t: time expressed in seconds, relative to the time of the first scan.
        
        scan_i = dataL.Scans(:,j);
        OOIs = MyProcessingOfScan(scan_i,t,MyGUIHandles,j);   % some function to use the data...
        set(MyGUIHandles.handle6,'xdata',OOIs.Centers(1,:),'ydata', OOIs.Centers(2,:));
        pause(0.01) ;


while 1             % in this example I skip some of the laser scans.
    
    if (ABCD.flagPause), pause(0.2) ; continue ; end
    if(ABCD.flagEnd)
        close();
        break;
    end
    
    if j>N
        break ;
    end
    
  
    
    if(IMUTime(i) > Laser_time(j))
        % Native time expressed via uint32 numbers, where 1 unint means
        % [1/10,000]second (i.e. 0.1 millisecond)
        % (BTW: we do not use the time, in this task)
        t =  Laser_time(j);
        % t: time expressed in seconds, relative to the time of the first scan.
        
        scan_i = dataL.Scans(:,j);
        OOIs = MyProcessingOfScan(scan_i,t,MyGUIHandles,j);   % some function to use the data...
        
        
        
        pause(0.01) ;                   % wait for ~10ms (approx.)
        j=j+1;
    end
    
    dt = IMUTime(i+1) - IMUTime(i);
    angle(i+1) = angle(i) + dt*(Yaw(i));
    X(i+1) = X(i) + dt*Speed(i) * cos(angle(i));
    Y(i+1) = Y(i) + dt*Speed(i) * sin(angle(i));
    
    set(MyGUIHandles.handle5,'xdata',X,'ydata', Y);
    %set(MyGUIHandles.handle7,'xdata',OOIs.Centers(1,:),'ydata', OOIs.Centers(2,:));
    i=i+skip;
    
    
end

fprintf('\nDONE!\n');

% Read Matlab Help for explanation of FOR loops, and function double( ) and pause()


return;



end

function OOIs = MyProcessingOfScan(scan,t,mh,i)
% I made this function, to receive the following parameters/variables:
% 'scan' : scan measurements to be shown.
% 't':  associated time.
% 'i' : scan number.
% 'mh'  : struct contaning handles of necessary graphical objects.

angles = [0:360]'*0.5* pi/180 ;              % Associated angle for each range of scan
% same as in "dataL.angles".

% scan data is provided as a array of class uint16, which encodes range
% and intensity (that is the way the sensor provides the data, in that
% mode of operation)

MaskLow13Bits = uint16(2^13-1); % mask for extracting the range bits.
% the lower 13 bits are for indicating the range data (as provided by this sensor)
maskE000 = bitshift(uint16(7),13)  ;
rangesA = bitand(scan,MaskLow13Bits) ;
% rangesA now contains the range data of the scan, expressed in CM, having uint16 format.
intensities = bitand(scan,maskE000);
% now I convert ranges to meters, and also to floating point format
ranges    = 0.01*double(rangesA);
X = cos(angles).*ranges;
Y = sin(angles).*ranges;
ii = find(intensities~=0);


OOIs = ExtractOOIs(ranges,intensities);
% % and then refresh the data in the plots...
set(mh.handle1,'xdata',X,'ydata',Y);
set(mh.handle2,'xdata',X(ii),'ydata',Y(ii));
set(mh.handle3,'xdata',OOIs.Centers(1,:),'ydata', OOIs.Centers(2,:));
set(mh.handle7,'xdata',OOIs.Centers(1,:),'ydata', OOIs.Centers(2,:));
% and some text...
s= sprintf('Laser scan # [%d] at time [%.3f] secs',i,t);
set(mh.handle4,'string',s);







return;
end


function OOIs = ExtractOOIs(ranges,intensities)
OOIs.N = 0;
OOIs.Centers = [];
OOIs.Sizes   = [];
OOIs.colour = [];
% your part....

angles = [0:360]'*0.5* pi/180 ;         % associated angle, for each individual range in a scan
X = cos(angles).*ranges;
Y = sin(angles).*ranges;
A = [X Y];

Dist_btw_pts = sqrt(sum(abs(diff(A)).^2,2));


cluster_index = [0 (find(Dist_btw_pts > 0.075))'];
OOIs.N = length(cluster_index) -1;

for i = 1:OOIs.N
    temp_cluster_x = X(cluster_index(i) + 1:cluster_index(i+1));
    temp_cluster_y = Y(cluster_index(i) + 1:cluster_index(i+1));
    [OOIs.Centers(:,i), OOIs.Sizes(i)]=circle_fit(temp_cluster_x, temp_cluster_y);
    
    
    if any(intensities(cluster_index(i) + 1:cluster_index(i+1))) > 0
        OOIs.Colour(i) = 1;
    else
        OOIs.Colour(i)=0;
    end
    
    if OOIs.Sizes(i) < 0.05 || OOIs.Sizes(i) > 0.20 || OOIs.Colour(i) == 0
        OOIs.Sizes(i) = 0;
        
    end
    
end
t =(find(OOIs.Sizes == 0));

OOIs.Centers(:,t) = [];
OOIs.Sizes(t) = [];
OOIs.Colour(t) = [];
return;
end


function [c,d] = circle_fit(x,y)

c = [mean(x) mean(y)];
d = sqrt(range(x)^2+range(y)^2);

end


function MyCallBackA(~,~,x)
global ABCD;

if (x==1)
    ABCD.flagPause = ~ABCD.flagPause; %Switch ON->OFF->ON -> and so on.
    return;
end;
if (x==2)
    ABCD.flagEnd = ~ABCD.flagEnd;
    disp('you pressed "END NOW"');
    %         uiwait(msgbox('Ooops, you still need to implement this one!','?','modal'));
    
    % students complete this.
    return;
end;
return;
end