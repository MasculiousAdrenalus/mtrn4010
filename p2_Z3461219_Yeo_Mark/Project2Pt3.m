% MTRN4010 Project 2 Task 1
% Mark Yeo; mark.yeo@student.unsw.edu.au
% Last modified 2016/06/02





function main()
    clc();
    stepFlag = false;
%     
%     % Init Possum API
%     AAA = API_PossumUGV2(0);
%     if (AAA.Ok<1), return ; end;
%     AAA.More.SendCmdToSimulator('restart');
%     AAA.More.SendCmdToSimulator('jump 15');
% 
% 
%     % Flush API buffers
%     rIm = AAA.ReadIMU(1,1) ;         % read last measurement, from IMU #1
%     rLa = AAA.ReadLMS200(2,1) ;      % from laser unit#2, get just 1 scan (last arrived one)
%     rSp  =AAA.ReadSpeed(1,1) ;        % read last one, from speed sensor #1

    
    
    % Init variables
    
    startTime = double(rIm.tcx(1,rIm.n))/10000;
    prevTime = 0.05;
    gBiasData = [];
    gBias = 0;
    k = 180/pi;
    prevYaw = -90/k;    %deg of initial heading
    prevPosn = [0;0];
    posnHist = [];
    initcentres = [];
    measuredSpeedHist = 0;
    pause(0.05);
    
    
    
    
    
    
    % CONSTANTS
    stdDevGyro = 2*pi/180 ;        
    stdDevSpeed = 0.3 ;
    sdev_rangeMeasurement = 0.1 ;
    sdev_bearingMeasurement = 0.5*pi/180;
    oldDt=0.05 ;                       % "sample time", 50ms
    Li = 5000 ;                     % "experiment" duration, in iterations 
    DtObservations=0.250 ;          % laser "sample time" (for observations), 4Hz, approximately








    % EKF Model Setup
    Xe = [0;
          0;
          pi/2;
          0];
    P = zeros(4,4); % b/c we assume we know the initial position perfectly
    Xe_History= Xe;
    %Q = diag( [ (0.01)^2 ,(0.01)^2 , (1*pi/180)^2]);    % covariance of uncertainty of process model
    time=0;
    disp('Running full simulation, ONLINE. Please wait...');


    plotHandles = InitPlots() ;
    
    global NavigationMap;
    NavigationMap = CreateMap(rLa);




    for frameno=1:9000,
        pause(0.1);

        tic();
        % read all sensors
        rIm = AAA.ReadIMU(1,100) ;      % read IMU unit #1, up to 100 samples
        rLa = AAA.ReadLMS200(2,15) ;    % read laser unit#2, get just L scans (last L arrived ones)
        rSp  =AAA.ReadSpeed(1,100) ;    % read up to 100 speed measurements    
        
        nowTime = double(rIm.tcx(1,rIm.n))/10000 - startTime;
        if frameno == 1
            nowTime = 0;
        end
        dt = nowTime - prevTime;
        prevTime = nowTime;

        
        % Get Process Model Inputs (gyro + speed)
        gyroZ = mean(double(rIm.data(6,1:rIm.n)));        
        caltime = 3; %sec
        %do calibration here
        if nowTime < caltime
            gBiasData = [gBiasData gyroZ];
            gBias = mean(gBiasData);
        end
        measuredSpeed = mean(double(rSp.data(1:rSp.n)));
        
        

        



        % EKF
        %1. Apply prediction step (Process model)
        Xe    = RunProcessModel(Xe,Xe(4),gyroZ,dt);
        %2. Estimate new covariance after prediction
        J = [   [1,0,-dt*Xe(4)*sin(Xe(3)),  dt*cos(Xe(3))];
                [0,1,dt*Xe(4)*cos(Xe(3)),   dt*sin(Xe(3))];
                [0,0,1,                     0];
                [0,0,0,                     1]];
        % a) Calculate better Q
        Pu = diag([stdDevGyro^2]);
        Ju = [  0;
                0;
                dt;
                0];  % (as a side thought, how does these values differ from discrete changeX/changeU?)
        Qu = Ju*Pu*Ju';
        Q1 = dt^2*diag( [ (0.01)^2 ,(0.01)^2 , (1*pi/180)^2, (1.2)^2]) ;
        Q = Q1 + Qu;
        % b) Calculate new covariance (P(K+1|K) = J*P(K|K)*J'+Q);
        P = J*P*J'+Q ;
        % Prediction step is done

        
        
        
        
        
        
        
        % Get range measurements
        [nDetectedLandmarks,MeasuredRanges,MeasuredBearings,IDs, gCentres]=GetObservationMeasurements(rLa, Xe);
        plotOOIs(gCentres, IDs, plotHandles);
        
        if size(IDs)>0,

            % Perform EKF update for each of the observations
            % Observation function is non-linear -> need to get Jacobians of h(X)
            for u=1:size(IDs),


                % For each observation, we calculate the expected observation

                ID = IDs(u);            % landmark ID?    
                eDX = (NavigationMap.landmarks(ID,1)-Xe(1)) ;      % (xu-x)
                eDY = (NavigationMap.landmarks(ID,2)-Xe(2)) ;      % (yu-y)
                eDD = sqrt( eDX*eDX + eDY*eDY ) ; %   so : sqrt( (xu-x)^2+(yu-y)^2 ) 
                eBB = atan2(eDY,eDX) - Xe(3) + pi/2;

                % H used to transform observations to state space + vice versa
                H = [  -eDX/eDD,    -eDY/eDD,   0, 0;
                        eDY/eDD^2,  -eDX/eDD^2, -1,0]; % Jacobian of h(X); size 2x3

                % Expected observations "h(Xe)"
                ExpectedRange = eDD ;   % just a coincidence: we already calculated them for the Jacobian, so I reuse it. 
                ExpectedBearing = eBB;
                
                if true && (stepFlag == true || MeasuredBearings(u)-ExpectedBearing > 0.4)
                    AAA.More.SendCmdToSimulator('p');  % also: to pause simulator,
                    stepFlag = true;
                    IDs(u)
                    ExpectedRange
                    MeasuredRanges(u)
                    ExpectedBearing
                    MeasuredBearings(u)
                    1;
                    AAA.More.SendCmdToSimulator('c');  % and to continue.  
                end
                
                % Residual (innovation) "Y-h(Xe)" (measured output value - expected output value)
                Z = [MeasuredRanges(u)-ExpectedRange; MeasuredBearings(u)-ExpectedBearing];
                
                if Z(2) > pi
                    Z(2) = Z(2) - 2*pi;
                elseif Z(2) < -pi
                    Z(2) = Z(2) + 2*pi;
                end

                % Covariance of noise/uncetainty in measurements
                R = diag([sdev_rangeMeasurement^2*4, sdev_bearingMeasurement^2*4]); % x4 to be conservative (2x stddev)

                % Some intermediate steps for the EKF (as presented in the lecture notes)
                S = R + H*P*H' ;
                iS=inv(S);              % iS = inv(S) ;   % in this case S is 1x1 so inv(S) is just 1/S
                K = P*H'*iS ;           % Kalman gain
                % Calculate  X(k+1|k+1) and P(k+1|k+1)
                Xe = Xe+K*Z ;           % update the expected value
                P = P-P*H'*iS*H*P ;     % update the covariance of e.v.
                % EKF update for single observation done
           end;
           % EKF update for all observations done    
        end;  
        % EKF update for all observations done (if any)


        % Store state to plot
         Xe_History = [Xe_History Xe] ;
         % Plot Xreal, Xe, landmarks
        plotPosition(Xe_History, plotHandles) ;
        plotLandmarks(NavigationMap, plotHandles);
        measuredSpeedHist = [measuredSpeedHist measuredSpeed];
        plotSpeed(Xe_History(4,:),measuredSpeedHist,nowTime,plotHandles)
    end;
      


return;








% Process Model
function Xnext=(X,speed,GyroZ,dt) 
    Xnext = X + dt*[ speed*cos(X(3)) ;  speed*sin(X(3)) ; GyroZ ; 0] ;
return ;


% Get map
function map = CreateMap(rLa)

    lscan = rLa.Ranges(:,rLa.n);

    centres = findShinyOOIs(lscan);
    
    if ~isempty(centres)
        map.landmarks = centres';
        map.nLandmarks = size(map.landmarks,1);
    end


return ;


% Get observation measurements
function [nDetectedLandmarks,MeasuredRanges,MeasuredBearings,IDs, gCentres]=GetObservationMeasurements(rLa, Xe)
    
    nDetectedLandmarks=0;
    MeasuredRanges=[];
    MeasuredBearings=[];
    IDs=[];
    gCentres = [];

    if isfield(rLa, 'Ranges')
        lscan = rLa.Ranges(:,rLa.n);

        centres = findShinyOOIs(lscan);
        if ~isempty(centres)
            dx = centres(1,:);
            dy = centres(2,:);
            nDetectedLandmarks = size(centres,2);
            MeasuredRanges = sqrt((dx.*dx + dy.*dy)) ;
            MeasuredBearings = atan2(dy,dx);
            gCentres = globaliseScan(centres, Xe);
            IDs = idcentres(gCentres, MeasuredRanges);
        end
    end
 return;


function centres = findShinyOOIs(scan)
    mask1FFF = uint16(2^13-1);
    maskE000 = bitshift(uint16(7),13)  ;
    intensities = bitand(scan,maskE000);
    ranges    = single(bitand(scan,mask1FFF))*0.01; 
    
    OOIs = findAllOOIs(ranges,intensities) ;
    OOIs.Centers(2,:) = OOIs.Centers(2,:) + 0.46;
    
    ii = find(intensities~=0);  % intense points
    plotLaserPoints(ranges, ii, OOIs);
    
    centresX = [];
    centresY = [];
    if OOIs.N>=1
        for i=1:OOIs.N
            if OOIs.Colors(i) == 1
                centresX = [centresX, OOIs.Centers(1,i)];
                centresY = [centresY, OOIs.Centers(2,i)];
            end
        end
    end
    %centresY = centresY + 0.46;
    centres = [centresX; centresY];
return;


function r = findAllOOIs(ranges,intensities)
    r.N = 0;
    r.Colors = [];
    r.Centers = [];
    r.Diameters = [];
    
    % 2D points, expressed in Cartesian. From the sensor's perpective.
    angles = [0:360]'*0.5* pi/180 ;         % associated angle for each range of scan
    X = cos(angles).*ranges;
    Y = sin(angles).*ranges;  
    
    numpts = 361;
    grpthresh = 0.1; % threshold distance for point clustering
    grpX = X(1);
    grpY = Y(1);
    shiny = intensities(1);
    for i=2:numpts
        % cluster adjacent points until non-adjacent point found
        ptdist = sqrt((X(i)-X(i-1))^2+(Y(i)-Y(i-1))^2);
        if ptdist < grpthresh   % if within cluster threshold:
            grpX = [grpX X(i)];
            grpY = [grpY Y(i)];
            shiny = shiny + intensities(i);
        else                    % if outside of cluster threshold:
            if size(grpX, 2) >= 3  % process last group
                % circle fit
                [xc,yc,R] = circlefit(grpX,grpY);
                % record centre & r if correct size object & shiny
                dist = sqrt(xc^2+yc^2);
                maxsize = 0.3;
                if (2*R > 0.05) && (2*R < maxsize)
                    r.N = r.N + 1;
                    r.Centers(:,r.N) = [xc yc];
                    r.Diameters(r.N) = R*2;
                    r.Colors(r.N) = 0;
                    if shiny > 0
                        r.Colors(r.N) = 1;
                    end
                end
            end
            grpX = [X(i)];      % overwrite last group w/ current point
            grpY = [Y(i)];
            shiny = 0;
        end
    end
return;


function [xc, yc,radius_exp] = circlefit(X,Y)
    %Author: Karan Narula 16/03/2016
    %obtain a set of equations to obtain the experimental center and radius
    X = X';
    Y = Y';
    A = -2*[X(1:end-1) - X(2:end), Y(1:end-1)-Y(2:end)];
    C = Y(2:end).^2 - Y(1:end-1).^2 + X(2:end).^2 - X(1:end-1).^2;
    center_exp = A\C;
    xc = center_exp(1);
    yc = center_exp(2);
    radius_exp = mean(sqrt((X - center_exp(1)).^2 + (Y - center_exp(2)).^2));
return;


function gcentres = globaliseScan(centres, Xe)
    %transform pole centres to global coordinate frame
    position = Xe(1:2);
    yaw = Xe(3);

    gcentres = centres;
    yaw = yaw-(90*pi/180);
    yaw = -yaw;
    R = [cos(yaw), -sin(yaw); sin(yaw), cos(yaw)];
    
    for i=1:size(gcentres,2)
        gcentres(:,i) = R' * gcentres(:,i);
        gcentres(:,i) = gcentres(:,i) + [position(1); position(2)];
    end
return;
 

function ids = idcentres(gcentres, MeasuredRanges)
    global NavigationMap;
    initcentres = NavigationMap.landmarks;
    ids = zeros(size(gcentres,2),1);

    % Associate closest risky points to landmarks
    numLM = size(initcentres,1);
    polIDs = zeros(5,1);
    if ~isempty(gcentres)
        lmAndPols = [initcentres; gcentres'];
        D1 = pdist(lmAndPols,'euclidean');
        D2 = squareform(D1);
        D3 = D2(1:numLM,numLM+1:end);
        
        for lmi=1:numLM
            % for each landmark,
            % find the minimum-distance pole #'poli'
            D4 = D3(lmi,:);
            %retryFlag = true;
            %while retryFlag == true
                %retryFlag = false;
                [m,poli] = min(D4);

                % then see if that pole is the minimum for another landmark
                lmis = 1:numLM;
                lmis(lmi) = [];
                isAssoc = true;
                for lmi2=lmis
                    D5 = D3(lmi2,:);
                    [m2,poli2] = min(D5);
                    if poli == poli2 && m > m2
                        % if so, don't associate that pole
                        isAssoc = false;
                        %D4(poli) = 999999;
                        %retryFlag = true;
                    end
                end
            %end
            % if not, associate the pole w/ that landmark
            if isAssoc
                % but if the pole is too far away from the closest
                % landmark, make a new landmark
                newLmDist = 0.6/1.5;
                currLmPlErr = m/MeasuredRanges(poli);
                if currLmPlErr > newLmDist
                    NavigationMap.landmarks = [NavigationMap.landmarks; gcentres(:,poli)'];
                    NavigationMap.nLandmarks = NavigationMap.nLandmarks + 1;
                    lmi = NavigationMap.nLandmarks;
                end
                polIDs(poli) = lmi;
            end
        end
        fltr = (polIDs ~= 0);
        ids = polIDs(fltr);
    end
return;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

% Plot results

function handles = InitPlots()
    figure(1) ; clf ; hold on ;
    handles.landmarks = plot(0,0,'*g') ;
    handles.observations = plot(0,0,'*r') ;
    handles.path = plot(0,0,'b.') ;
    %ii = [1:225:length(Xe_History(1,:))] ;
    %quiver(Xe_History(1,ii),Xe_History(2,ii),5*cos(Xe_History(3,ii)),5*sin(Xe_History(3,ii)),'r' ) ;
    %plot(Xe_History(1,ii),Xe_History(2,ii),'+r') ;
    title('Trajectory according to EKF') ;
    
    handles.label(1) = text(0, 0, '');
    handles.label(2) = text(0, 0, '');
    handles.label(3) = text(0, 0, '');
    handles.label(4) = text(0, 0, '');
    handles.label(5) = text(0, 0, '');
    axis equal;
    axis([-6,4,-2,8]);
    xlabel('X (meters)');
    ylabel('Y (meters)');
    zoom on ;  grid on;
    
    
    
    
    global LaserHandles;
    
    figure(2) ; clf(); hold on;
    LaserHandles.all = plot(0,0,'b.');
    LaserHandles.shiny = plot(0,0,'r.');
    LaserHandles.OOIs = plot(0, 0, '*', 'color', [0,0.7,0]);  % pole centres
    axis equal;
    axis([-7,7,0,8]);
    xlabel('X (meters)');
    ylabel('Y (meters)');
    zoom on ;  grid on;

    
    figure(3); clf(); hold on;
    handles.calcSpeed = plot(0,0,'b');
    handles.measuredSpeed = plot(0,0,'r');
    
return ;

function plotLandmarks(map, h)
    set(h.landmarks, 'xdata',-map.landmarks(:,1), 'ydata',map.landmarks(:,2)) ;
return ;

function plotOOIs(gCentres, ids, h)
    if size(gCentres,2) == 0
        set(h.observations, 'xdata',[], 'ydata',[]) ;
    else
        set(h.observations, 'xdata',-gCentres(1,:), 'ydata',gCentres(2,:)) ;
    end

    set(h.label(1),'string','');
    set(h.label(2),'string','');
    set(h.label(3),'string','');
    set(h.label(4),'string','');
    set(h.label(5),'string','');
    for i=1:size(ids)
        if ids(i) >= 1 && ids(i) <= 5
            s = sprintf('Pole %d',ids(i));
            set(h.label(ids(i)),'position',[-gCentres(1,i),gCentres(2,i)],'string',s);
        end
    end
    
return ;

function plotPosition(Xe_History,h)
    set(h.path, 'xdata',-Xe_History(1,:), 'ydata',Xe_History(2,:)) ;
return ;

function plotLaserPoints(ranges, ii, OOIs)
    global LaserHandles;
    
    angles = [0:360]'*0.5* pi/180 ;
    X = cos(angles).*ranges;
    Y = sin(angles).*ranges + 0.46;    
    
    
    centresX = [];
    centresY = [];
    if OOIs.N>=1
        for i=1:OOIs.N
            if OOIs.Colors(i) == 1
                centresX = [centresX, OOIs.Centers(1,i)];
                centresY = [centresY, OOIs.Centers(2,i)];
            end
        end
    end
    
    % refresh the data in the plots
    set(LaserHandles.all,'xdata',-X,'ydata',Y);
    set(LaserHandles.shiny,'xdata',-X(ii),'ydata',Y(ii));
    set(LaserHandles.OOIs,'xdata',-centresX,'ydata',centresY);
return;
    

function plotSpeed(speedHistory,measuredSpeed,tNow,h)
    tdata = linspace(0,tNow,size(speedHistory,2));
    set(h.calcSpeed, 'xdata',tdata, 'ydata',speedHistory) ;
    set(h.measuredSpeed, 'xdata',tdata, 'ydata',measuredSpeed) ;
return ;
