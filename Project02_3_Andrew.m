

function Project02_Final

    load('DataForProject02/IMU_dataC.mat');
    load('DataForProject02/Laser__2C.mat');
    load('DataForProject02/Speed_dataC.mat');
    
    

    global Master;
    Master.Case = 0;
    Master.Threshold = 0.08;

    H = IniPlots();

    % IMU
    Data.IMU.Time = double((IMU.times - IMU.times(1)))*0.0001;
    Data.IMU.N = IMU.N;
    Data.IMU.YawRate = -IMU.DATAf(6,:);
    % Velocity
    Data.Vel = Vel.speeds;
    % Laser Data
    Data.Laser.Time = double((dataL.times - dataL.times(1)))*0.0001;
    Data.Laser.Scans = dataL.Scans;
    Data.Laser.Angles = dataL.angles;
    CurrentScan = 1;
    % Initialisation
    Gyro = [0 0 pi/2];
    Yaw = zeros(Data.IMU.N, 1);
    X = zeros(Data.IMU.N, 1);
    Y = zeros(Data.IMU.N, 1);
    Yaw(1) = Gyro(3);
    
    for i=2:Data.IMU.N
        if Data.IMU.Time(i) < 17.0
            bias = mean(Data.IMU.YawRate(1:i));
        end
        dt = Data.IMU.Time(i) - Data.IMU.Time(i-1);
        Yaw(i) = Yaw(i-1) + dt*(Data.IMU.YawRate(i-1)-bias);
        X(i) = X(i-1) + dt*Data.Vel(i-1)*cos(Yaw(i-1));
        Y(i) = Y(i-1) + dt*Data.Vel(i-1)*sin(Yaw(i-1));
    end
    
    set(H.position, 'xdata', X, 'ydata', Y);
    set(H.heading, 'xdata', Data.IMU.Time, 'ydata', rad2deg(Yaw));
    set(H.angrate, 'xdata', Data.IMU.Time, 'ydata', Data.IMU.YawRate-bias);

    % Processting Time 0 Data
    [R, I] = ExtractLaserData(Data.Laser.Scans(:,1));
    Data.Laser.X = -cos(Data.Laser.Angles).*R;
    Data.Laser.Y =  sin(Data.Laser.Angles).*R + 0.46;
    
    Globals = ExtractOOIs(Data.Laser.X, Data.Laser.Y, I, Master);
    Globals.Centers = Globals.Centers(:,find(Globals.Colours));
    
    Globals.Centers = Transform(Yaw(1)-pi/2, X(1), Y(1),...
        Globals.Centers(1,:), Globals.Centers(2,:));
    
    % Global OOIs at T = 0;
    set(H.Gs, 'xdata', Globals.Centers(1,:), 'ydata', Globals.Centers(2,:));
      
    for i=2:Data.IMU.N
  
        if Data.IMU.Time(i) < 17.0
            bias = mean(Data.IMU.YawRate(1:i));
        end
        dt = Data.IMU.Time(i) - Data.IMU.Time(i-1);
        Yaw(i) = Yaw(i-1) + dt*(Data.IMU.YawRate(i-1)-bias);
        X(i) = X(i-1) + dt*Data.Vel(i-1)*cos(Yaw(i-1));
        Y(i) = Y(i-1) + dt*Data.Vel(i-1)*sin(Yaw(i-1));
        
        if (Data.IMU.Time(i) > Data.Laser.Time(CurrentScan) && Data.IMU.Time(i) <= Data.Laser.Time(end))
            CurrentScan = CurrentScan + 1;
            Alpha = Yaw(i-1) - pi/2;
            
            [R, I] = ExtractLaserData(Data.Laser.Scans(:,CurrentScan));
            Data.LaserX = -cos(Data.Laser.Angles).*R;
            Data.LaserY =  sin(Data.Laser.Angles).*R + 0.46;
            % Extract Local Coordiante Data
            Locals = ExtractOOIs(Data.LaserX, Data.LaserY, I, Master);
            Locals.Centers = Locals.Centers(:, find(Locals.Colours));
            
            Locals.Centers = Transform(Alpha, X(i), Y(i), Locals.Centers(1,:),...
                Locals.Centers(2,:));
            set(H.Ls, 'xdata', Locals.Centers(1,:), 'ydata', Locals.Centers(2,:));
            % Current Laserscan Data
            set(H.LaserScan, 'xdata', 0, 'ydata', 0);
            DA(H, Globals, Locals);
        end
        
        set(H.Veh, 'xdata', X(1:i-1), 'ydata', Y(1:i-1));
        pause(0.008);
    end
end

function DA(h, g, l)
    X = ones(2, g.Identity);
    Y = ones(2, g.Identity);

    for j=1:g.Identity
        distx = g.Centers(1,j) - l.Centers(1,:);
        disty = g.Centers(2,j) - l.Centers(2,:);

        dist = sqrt(distx.^2 + disty.^2);
        [mindist, id] = min(dist);

        if mindist < 0.4
            set(h.L_Labels(j),'Position',[g.Centers(1,j) + 0.4,...
                g.Centers(2,j)],'String',...
                ['Error: ',num2str(mindist,'%f m')]);
        else
            set(h.L_Labels(j),'Position',[0 0],'String','');
        end
          
    end
end

function result = Transform(alpha, Xr, Yr, X, Y)
    result = [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)]*...
        [X; Y];
    result = result + [Xr; Yr];
end

function [Ranges, Intensities] = ExtractLaserData(scans)
    MaskLow13Bits   = uint16(2^13-1);
    MaskHigh13Bits  = bitshift(uint16(7), 13);
    RangesA         = bitand(scans, MaskLow13Bits);
    Ranges          = 0.01*double(RangesA);
    Intensities     = bitand(scans, MaskHigh13Bits);
end

function m = IniPlots()
    global Master;

    m.h2 = figure(1); clf(); 
    m.sb1 = plot(0,0,'b'); 
    m.heading = plot(0,0,'b'); grid on;
    xlabel('Time (seconds)'); ylabel('Yaw (degrees)');
    m.h4 = figure(2);
    m.sb2 = plot(0,0,'b');
    m.position = plot(0,0,'b'); grid on; hold on;
    xlabel('X (meters)'); ylabel('Y (meters)');
    m.h6 = figure(3);
    m.sb3 = plot(0,0,'b'); hold on;
    m.angrate = plot(0,0,'b'); grid on;
    xlabel('Time (seconds)'); ylabel('Yaw (rads/s)'); 

    m.h1 = figure(4); clf(); hold on;
    m.Veh = plot(0,0,'c.');
    m.LaserScan = plot(0,0,'w.'); 
    m.Gs = plot(0,0,'or','LineWidth',0.8,'MarkerSize',6);
    m.Ls = plot(0,0,'xg','LineWidth',1.2,'MarkerSize',8); 

    m.L_Labels = text(zeros(1,5),zeros(1,5),'','Color','k'); 

    xlabel('X (meters)'); ylabel('Y (meters)'); grid on;

    axis([-10, 10, -10, 10]);

    zoom on;
      
end

