
function brycegossling(file)

global ABCD;
ABCD.flagPause=0;
% In case the caller does not specify the input argument, we propose a
% default one, assumed to be in the same folder where we run this example from.
% if ~exist('file','var'), file ='DataForProject02/Laser__2C.mat'; load(file);end;
% if ~exist('file','var'), file ='DataForProject02/IMU_dataC.mat'; load(file); end;
% if ~exist('file','var'), file ='DataForProject02/Speed_dataC.mat'; load(file); end;
load('DataForProject02/IMU_dataC.mat','IMU');
load('DataForProject02/Speed_dataC.mat','Vel');
load('DataForProject02/Laser__2C.mat', 'dataL');

%imu setup init
time_imu = double(IMU.times(:)-IMU.times(1))/10000;
dat = double( -(IMU.DATAf(6,:) - mean( IMU.DATAf(6,1:1000))) );
N_imu = length(time_imu)-1;

angularPos = zeros(N_imu,1);
angularPos(1) = pi/2;

% for k = 1:N_imu
%     dt = time_imu(k+1) - time_imu(k);
%     angularPos(k+1) = angularPos(k) + dt*(dat(k));
% end
% plot(time_imu, dat);
% plot(time_imu, angularPos);
% return;

time_laser =  double(dataL.times(:)-dataL.times(1))/10000;
N_laser = dataL.N;

r.N = 0;
r.Centers = [];
r.Sizes   = [];
OOIhr = [];


%speed data setup
Speed = Vel.speeds;
X = zeros(Vel.N,1);
Y = zeros(Vel.N,1);



% --------------------------------------
% Create graphical object for refreshing data during program execution.
figure(1) ; clf();

MyGUIHandles.plot1 = plot(0,0,'b.',0,0,'r.',0,0,'go',0,0,'m.');      % to be used for showing the laser points
hold on;
                   % focuses the plot on this region (of interest, close to the robot)
axis([-10,10,0,20]);                        % focuses the plot on this region (of interest, close to the robot)
xlabel('X (meters)');
ylabel('Y (meters)');

MyGUIHandles.plot1_title = title('');           % create an empty title..
zoom on ;  grid on;

fprintf('\nThere are [ %d ] laser scans in this dataset (file [%s])\n',dataL.N,'Laser__2C.mat');

uicontrol('Style','pushbutton','String','Pause/Cont.','Position',[10,1,80,20],'Callback',{@MyCallBackA,1});
uicontrol('Style','pushbutton','String','END Now','Position',[90,1,80,20],'Callback',{@MyCallBackA,2});

figure(2); clf();
grid on; hold on;
MyGUIHandles.plot2 = plot(0,0,'m.', 0,0,'go');
%axis([-8,8,-8,8]);
% Now, loop through the avaialable scans..


skip=3;


%scan first frame
scan_i = dataL.Scans(:,1);
scan = ExtractScan(scan_i);
r = ExtractOOI(scan.ranges, scan.intensity, MyGUIHandles);
OOIhr = ExtractOOIHR(r);
set(MyGUIHandles.plot2(2),'xdata',OOIhr.x,'ydata', OOIhr.y);

j=1;
for i = 1:N_imu             % in this example I skip some of the laser scans.
    if (ABCD.flagPause), pause(0.2) ; continue ; end;
    if i>N_imu, break ;  end;
%     if j>N_laser, break ;  end;
    
    
    if (time_laser(j) < time_imu(i))
        scan_i = dataL.Scans(:,j);
        scan = ExtractScan(scan_i);
        tic;
        r = ExtractOOI(scan.ranges, scan.intensity, MyGUIHandles);   % some function to use the data...
        toc;
        PlotScan(scan.ranges, scan.intensity, r, MyGUIHandles,j, time_laser(j));
        pause(0.01);
        j= j+1;
    end

    dt = time_imu(i+1) - time_imu(i);
    angularPos(i+1) = angularPos(i) + dt*(dat(i));
    X(i+1) = X(i) + dt*Speed(i) * cos(angularPos(i));
    Y(i+1) = Y(i) + dt*Speed(i) * sin(angularPos(i));

    assignin('base','angularPos', angularPos);
    assignin('base','X',X);
    assignin('base','Y',Y);
    assignin('base','r',r);
    %pause(0.01) ;                   % wait for ~10ms (approx.)
    i=i+skip;
    set(MyGUIHandles.plot2(1),'xdata',X,'ydata', Y);
    %set(MyGUIHandles.plot2(2),'xdata',r.Centers(1,:),'ydata', r.Centers(2,:));
end

    
    fprintf('\nDONE!\n');

return;
end
%-----------------------------------------
%%
function PlotScan(ranges, intensity, r, mh,i,t)
    angles = [0:360]'*0.5* pi/180 ;         % associated angle, for each individual range in a scan
    X = cos(angles).*ranges;
    Y = sin(angles).*ranges;
    OOI.x=[];
    OOI.y=[];
    set(mh.plot1(1),'xdata',X,'ydata',Y);
    OOI = ExtractOOIHR(r);
    set(mh.plot1(3),'xdata',OOI.x,'ydata',OOI.y);
    ii = find(intensity~=0);
    Xi = X(ii);
    Yi = Y(ii);
    
    set(mh.plot1(2),'xdata',Xi,'ydata',Yi);
    s= sprintf('Laser scan # [%d] at time [%.3f] secs',i,t);
    set(mh.plot1_title,'string',s);
    
end
%%
function OOI = ExtractOOIHR(r)
    OOI.x=[];
    OOI.y=[];
    ii = find(r.Color~=0);
    OOI.x = [OOI.x r.Centers(1,ii)];
    OOI.y = [OOI.y r.Centers(2,ii)];
    return;
end
%%
% ---------------------------------------
% Callback function. I defined it, and associated it to certain GUI button,
function MyCallBackA(~,~,x)
global ABCD;

if (x==1)
    ABCD.flagPause = ~ABCD.flagPause; %Switch ON->OFF->ON -> and so on.
    return;
end;
if (x==2)
    
    disp('you pressed "END NOW"');
    %uiwait(msgbox('Ooops, you still need to implement this one!','?','modal'));
    %set(x.start, 'UserData', true);
    % students complete this.
    close;
    %set(gcbo,'userdata',1);
    return;
end;
return;
end
